package com.main.movies;

import java.util.*;

public class MovieCatalogue {
    private static MovieCatalogue instance;
    private List<MoviePosition> positions;
    private MovieRentArchive archive;

    private MovieCatalogue() {
        this.positions = new LinkedList<>();
        this.archive = MovieRentArchive.getInstance();
    }

    public static MovieCatalogue getInstance() {
        if (instance == null) {
            instance = new MovieCatalogue();
        }
        return instance;
    }

    public void addMoviePosition(String title, Integer price, Integer quantity, String owner) {
        Optional<MoviePosition> first = positions.stream()
                .filter(p -> p.movie.title.equals(title))
                .filter(p -> p.owner.equals(owner))
                .findFirst();
        if (!first.isPresent()) {
            positions.add(new MoviePosition(title, price, quantity, owner));
        }
    }

    public void addMovieQuantity(String title, Integer quantity, String owner) {
        Optional<MoviePosition> first = positions.stream()
                .filter(p -> p.movie.title.equals(title))
                .filter(p -> p.owner.equals(owner))
                .findFirst();
        first.orElseThrow(() -> new RuntimeException("Movie does not exist in catalogue"))
                .quantity += quantity;
    }

    public boolean isMovieAvailable(String title) {
        return positions.stream()
                .filter(p -> p.movie.title.equals(title))
                .mapToInt(p -> p.quantity)
                .sum() > 0;
    }

    public int lowestCost(String title) {
        return positions.stream()
                .filter(p -> p.movie.title.equals(title))
                .filter(p -> p.quantity > 0)
                .min(Comparator.comparingInt(MoviePosition::getMoviePrice))
                .orElseThrow(() -> new RuntimeException("Movie is not available"))
                .movie
                .price;
    }

    public boolean rent(String title, String borrower) {
        Optional<MoviePosition> first = positions.stream()
                .filter(p -> p.movie.title.equals(title))
                .filter(p -> p.quantity > 0)
                .min(Comparator.comparingInt(MoviePosition::getMoviePrice));

        first.ifPresent(moviePosition -> {
            MoviePosition position = first.get();
            this.archive.addMovieRent(position.owner, borrower, position.movie.title);
            position.quantity--;
        });
        return first.isPresent();

    }

    public List<MoviePosition> getPositions() {
        return new ArrayList<>(positions);
    }
}
