package com.main.movies;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MovieRent {
    private String owner;
    private String borrower;
    private String movieTitle;
    private String timestamp;

    MovieRent(String owner, String borrower, String movieTitle) {
        this.owner = owner;
        this.borrower = borrower;
        this.movieTitle = movieTitle;
        this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
    }

    @Override
    public String toString() {
        return "MovieRent{" +
                "owner='" + owner + '\'' +
                ", borrower='" + borrower + '\'' +
                ", movieTitle='" + movieTitle + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
