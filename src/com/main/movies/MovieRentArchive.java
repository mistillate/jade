package com.main.movies;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MovieRentArchive {
    private static MovieRentArchive instance;
    private Map<String, List<MovieRent>> rents;

    private MovieRentArchive() {
        this.rents = new HashMap<>();
    }

    public static MovieRentArchive getInstance() {
        if (instance == null) {
            instance = new MovieRentArchive();
        }
        return instance;
    }

    void addMovieRent(String owner, String borrower, String movieTitle) {
        this.rents.putIfAbsent(borrower, new LinkedList<>());
        this.rents.get(borrower)
                .add(new MovieRent(owner, borrower, movieTitle));
    }

    public Map<String, List<MovieRent>> getRents() {
        return new HashMap<>(rents);
    }
}
