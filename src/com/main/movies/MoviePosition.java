package com.main.movies;

public class MoviePosition {
    Movie movie;
    Integer quantity;
    String owner;

    MoviePosition(String title, Integer price, Integer quantity, String owner) {
        this.movie = new Movie(title, price);
        this.quantity = quantity;
        this.owner = owner;
    }

    Integer getMoviePrice() {
        return this.movie.price;
    }

    @Override
    public String toString() {
        return "MoviePosition{" +
                "movie=" + movie +
                ", quantity=" + quantity +
                ", owner='" + owner + '\'' +
                '}';
    }
}
