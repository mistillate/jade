package com.main;

import com.main.guis.AdminGui;
import com.main.movies.MovieCatalogue;
import com.main.movies.MovieRentArchive;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

public class AdminAgent extends Agent {

    private MovieCatalogue movieCatalogue = MovieCatalogue.getInstance();
    private MovieRentArchive movieRentArchive = MovieRentArchive.getInstance();

    protected void setup() {
        AdminGui gui = new AdminGui(this);
        gui.showGui();

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("admin-movie-renting");
        sd.setName("JADE-admin-movie-renting");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    public void showCatalogue() {
        movieCatalogue.getPositions().forEach(System.out::println);
    }

    public void showRents() {
        movieRentArchive.getRents().forEach((s, movieRents) -> System.out.println(movieRents));
    }
}
