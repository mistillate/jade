package com.main;

import com.main.guis.MovieRentalGui;
import com.main.movies.MovieCatalogue;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class MovieRentalAgent extends Agent {
    private String name;
    private MovieCatalogue new_catalogue;
    private MovieRentalGui movieRentalGui;

    protected void setup() {
        this.name = getAID().getName();
        this.new_catalogue = MovieCatalogue.getInstance();
        this.movieRentalGui = new MovieRentalGui(this);
        this.movieRentalGui.showGui();

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("movie-renting");
        sd.setName("JADE-movie-renting");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        addBehaviour(new OfferRequestsServer());
        addBehaviour(new PurchaseOrdersServer());
    }

    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        movieRentalGui.dispose();
        System.out.println("MovieRental-agent " + getAID().getName() + " terminating.");
    }

    public void updateCatalogue(String title, Integer price, Integer quantity) {
        addBehaviour(new OneShotBehaviour() {
            public void action() {
                new_catalogue.addMoviePosition(title, price, quantity, name);
                System.out.println(title + " inserted into catalogue.");
            }
        });
    }

    private class OfferRequestsServer extends CyclicBehaviour {
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                String title = msg.getContent();
                ACLMessage reply = msg.createReply();

                if (new_catalogue.isMovieAvailable(title)) {
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(String.valueOf(new_catalogue.lowestCost(title)));
                } else {
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("not-available");
                }
                myAgent.send(reply);
            } else {
                block();
            }
        }
    }

    private class PurchaseOrdersServer extends CyclicBehaviour {
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                String title = msg.getContent();
                ACLMessage reply = msg.createReply();

                if (new_catalogue.rent(title, msg.getSender().getName() )) {
                    reply.setPerformative(ACLMessage.INFORM);
                    System.out.println(title + " rent to agent " + msg.getSender().getName());
                } else {
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("not-available");
                }
                myAgent.send(reply);
            } else {
                block();
            }
        }
    }
}
