package com.main.guis;

import com.main.MovieRentalAgent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MovieRentalGui extends Gui {
    private MovieRentalAgent agent;

    private JTextField titleField, priceField, quantityField;

    public MovieRentalGui(MovieRentalAgent agent) {
        super(agent.getLocalName());

        this.agent = agent;

        JPanel p = new JPanel();
        p.setLayout(new GridLayout(3, 2));

        p.add(new JLabel("Movie title:"));
        titleField = new JTextField(15);
        p.add(titleField);

        p.add(new JLabel("Price:"));
        priceField = new JTextField(15);
        p.add(priceField);

        p.add(new JLabel("Quantities:"));
        quantityField = new JTextField(15);
        p.add(quantityField);

        getContentPane().add(p, BorderLayout.CENTER);

        JButton addButton = new JButton("Add");
        addButton.addActionListener(ev -> {
            try {
                String title = titleField.getText().trim();
                String price = priceField.getText().trim();
                String quantity = quantityField.getText().trim();
                agent.updateCatalogue(title, Integer.parseInt(price), Integer.parseInt(quantity));
                titleField.setText("");
                priceField.setText("");
                quantityField.setText("");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(
                        MovieRentalGui.this,
                        "Invalid values. " + e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
        p = new JPanel();
        p.add(addButton);
        getContentPane().add(p, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                agent.doDelete();
            }
        });

        setResizable(false);
    }
}
