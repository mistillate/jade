package com.main.guis;

import com.main.AdminAgent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AdminGui extends Gui {

    public AdminGui(AdminAgent agent) {
        super(agent.getLocalName());

        JPanel p = new JPanel();
        p.setLayout(new GridLayout(3, 1));

        getContentPane().add(p, BorderLayout.CENTER);

        JButton showCatalogue = new JButton("Show catalogue");
        showCatalogue.addActionListener(ev -> agent.showCatalogue());

        JButton showRents = new JButton("Show rents");
        showRents.addActionListener(ev -> agent.showRents());

        p = new JPanel();
        p.add(showCatalogue);
        p.add(showRents);
        getContentPane().add(p, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                agent.doDelete();
            }
        });

        setResizable(false);
    }
}
